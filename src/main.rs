extern crate bincode;

mod cpu;
mod instructions;

use cpu::Cpu;

pub fn main() {
    let mut program = vec![
        0x2E,
    ];
    let mut cpu = Cpu::new();

    cpu.load(&mut program);
    cpu.run();
}
