use bincode::{deserialize, serialize};

use cpu::Cpu;

use std::mem::size_of;

pub static OFFSET: &[usize] = &[
    /*     |  0  |  1  |  2  |  3  |  4  |  5  |  6  |  7  |  8  |  9  |  a  |  b  |  c  |  d  |  e  |  f  |     */
    /* 0 */   1 ,   9 ,   9 ,   9 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 , /* 0 */
    /* 1 */   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 , /* 1 */
    /* 2 */   1 ,   1 ,   1 ,   1 ,   9 ,   9 ,   9 ,   9 ,   1 ,   1 ,   1 ,   9 ,   1 ,   1 ,   1 ,   1 , /* 2 */
    /* 3 */   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 , /* 3 */
    /* 4 */   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 , /* 4 */
    /* 5 */   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 , /* 5 */
    /* 6 */   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 , /* 6 */
    /* 7 */   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 , /* 7 */
    /* 8 */   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 , /* 8 */
    /* 9 */   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 , /* 9 */
    /* a */   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 , /* a */
    /* b */   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 , /* b */
    /* c */   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 , /* c */
    /* d */   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 , /* d */
    /* e */   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 , /* e */
    /* f */   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 ,   1 , /* f */
    /*     |  0  |  1  |  2  |  3  |  4  |  5  |  6  |  7  |  8  |  9  |  a  |  b  |  c  |  d  |  e  |  f  |     */
];

pub fn nop(_cpu: &mut Cpu) {
}

pub fn hlt(cpu: &mut Cpu) {
    cpu.rxx = false;
}

pub fn add(cpu: &mut Cpu) {
    cpu.rax = cpu.rax + cpu.rbx;
    cpu.set_arith_flag();
}

pub fn sub(cpu: &mut Cpu) {
    cpu.rax = cpu.rax - cpu.rbx;
    cpu.set_arith_flag();
}

pub fn mul(cpu: &mut Cpu) {
    cpu.rax = cpu.rax * cpu.rbx;
    cpu.set_arith_flag();
}

pub fn div(cpu: &mut Cpu) {
    cpu.rax = cpu.rax / cpu.rbx;
    cpu.set_arith_flag();
}

pub fn mol(cpu: &mut Cpu) {
    cpu.rax = cpu.rax % cpu.rbx;
    cpu.set_arith_flag();
}

pub fn sia(cpu: &mut Cpu) {
    cpu.rax = cpu.arg as isize;
}

pub fn sib(cpu: &mut Cpu) {
    cpu.rbx = cpu.arg as isize;
}

pub fn sic(cpu: &mut Cpu) {
    cpu.rcx = cpu.arg as isize;
}

pub fn lda(cpu: &mut Cpu) {
    cpu.rax = deserialize(&(*cpu.mem)[cpu.arg..(cpu.arg + size_of::<usize>())]).unwrap()
}

pub fn ldb(cpu: &mut Cpu) {
    cpu.rbx = deserialize(&(*cpu.mem)[cpu.arg..(cpu.arg + size_of::<usize>())]).unwrap()
}

pub fn ldc(cpu: &mut Cpu) {
    cpu.rcx = deserialize(&(*cpu.mem)[cpu.arg..(cpu.arg + size_of::<usize>())]).unwrap()
}

pub fn sta(cpu: &mut Cpu) {
    for (idx, v) in serialize(&cpu.rax).unwrap().iter().enumerate() {
        cpu.mem[cpu.arg + idx] = *v;
    }
}

pub fn stb(cpu: &mut Cpu) {
    for (idx, v) in serialize(&cpu.rbx).unwrap().iter().enumerate() {
        cpu.mem[cpu.arg + idx] = *v;
    }
}

pub fn stc(cpu: &mut Cpu) {
    for (idx, v) in serialize(&cpu.rcx).unwrap().iter().enumerate() {
        cpu.mem[cpu.arg + idx] = *v;
    }
}

pub fn jmp(cpu: &mut Cpu) {
    cpu.eip = cpu.arg;
}

pub fn beq(cpu: &mut Cpu) {
    if cpu.get_arith_flag(0) == 1 {
        cpu.eip = cpu.arg;
    }
}

pub fn bnz(cpu: &mut Cpu) {
    if cpu.get_arith_flag(0) == 0 {
        cpu.eip = cpu.arg;
    }
}

pub fn bgz(cpu: &mut Cpu) {
    if cpu.get_arith_flag(1) == 0 {
        cpu.eip = cpu.arg;
    }
}

pub fn blz(cpu: &mut Cpu) {
    if cpu.get_arith_flag(1) == 1 {
        cpu.eip = cpu.arg;
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn op_sta() {
        let mut program: Vec<u8> = Vec::new();
        let mut cpu = Cpu::new();

        for _idx in 0..size_of::<usize>() {
            program.push(0);
        }

        cpu.load(&mut program);
        cpu.rax = 5;

        sta(&mut cpu);

        assert_eq!([5, 0, 0, 0, 0, 0, 0, 0], &*cpu.mem);
    }

    #[test]
    fn op_sta_long() {
        let mut program: Vec<u8> = Vec::new();
        let mut cpu = Cpu::new();

        for _idx in 0..size_of::<usize>() {
            program.push(0);
        }

        cpu.load(&mut program);
        cpu.rax = 1200;

        sta(&mut cpu);

        assert_eq!(1200, cpu.rax);
        assert_eq!([0xb0, 0x04, 0, 0, 0, 0, 0, 0], &*cpu.mem);
    }

    #[test]
    fn op_sta_neg() {
        let mut program: Vec<u8> = Vec::new();
        let mut cpu = Cpu::new();

        for _idx in 0..size_of::<usize>() {
            program.push(0);
        }

        cpu.load(&mut program);
        cpu.rax = -1200;

        sta(&mut cpu);

        assert_eq!([0x50, 0xfb, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff], &*cpu.mem);
    }

    #[test]
    fn op_stb() {
        let mut program: Vec<u8> = Vec::new();
        let mut cpu = Cpu::new();

        for _idx in 0..size_of::<usize>() {
            program.push(0);
        }

        cpu.load(&mut program);
        cpu.rbx = 5;

        stb(&mut cpu);

        assert_eq!([5, 0, 0, 0, 0, 0, 0, 0], &*cpu.mem);
    }

    #[test]
    fn op_stb_long() {
        let mut program: Vec<u8> = Vec::new();
        let mut cpu = Cpu::new();

        for _idx in 0..size_of::<usize>() {
            program.push(0);
        }

        cpu.load(&mut program);
        cpu.rbx = 1200;

        stb(&mut cpu);

        assert_eq!([0xb0, 0x04, 0, 0, 0, 0, 0, 0], &*cpu.mem);
    }

    #[test]
    fn op_stb_neg() {
        let mut program: Vec<u8> = Vec::new();
        let mut cpu = Cpu::new();

        for _idx in 0..size_of::<usize>() {
            program.push(0);
        }

        cpu.load(&mut program);
        cpu.rbx = -1200;

        stb(&mut cpu);

        assert_eq!([0x50, 0xfb, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff], &*cpu.mem);
    }

    #[test]
    fn op_stc() {
        let mut program: Vec<u8> = Vec::new();
        let mut cpu = Cpu::new();

        for _idx in 0..size_of::<usize>() {
            program.push(0);
        }

        cpu.load(&mut program);
        cpu.rcx = 5;

        stc(&mut cpu);

        assert_eq!([5, 0, 0, 0, 0, 0, 0, 0], &*cpu.mem);
    }

    #[test]
    fn op_stc_long() {
        let mut program: Vec<u8> = Vec::new();
        let mut cpu = Cpu::new();

        for _idx in 0..size_of::<usize>() {
            program.push(0);
        }

        cpu.load(&mut program);
        cpu.rcx = 1200;

        stc(&mut cpu);

        assert_eq!([0xb0, 0x04, 0, 0, 0, 0, 0, 0], &*cpu.mem);
    }

    #[test]
    fn op_stc_neg() {
        let mut program: Vec<u8> = Vec::new();
        let mut cpu = Cpu::new();

        for _idx in 0..size_of::<usize>() {
            program.push(0);
        }

        cpu.load(&mut program);
        cpu.rcx = -1200;

        stc(&mut cpu);

        assert_eq!([0x50, 0xfb, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff], &*cpu.mem);
    }

    #[test]
    fn op_lda() {
        let mut program: Vec<u8> = vec![5, 0, 0, 0, 0, 0, 0, 0];
        let mut cpu = Cpu::new();

        cpu.load(&mut program);

        lda(&mut cpu);

        assert_eq!(5, cpu.rax);
    }

    #[test]
    fn op_lda_long() {
        let mut program: Vec<u8> = vec![0xb0, 0x04, 0, 0, 0, 0, 0, 0];
        let mut cpu = Cpu::new();

        cpu.load(&mut program);

        lda(&mut cpu);

        assert_eq!(1200, cpu.rax);
    }

    #[test]
    fn op_lda_neg() {
        let mut program: Vec<u8> = vec![0x50, 0xfb, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff];
        let mut cpu = Cpu::new();

        cpu.load(&mut program);

        lda(&mut cpu);

        assert_eq!(-1200, cpu.rax);
    }

    #[test]
    fn op_ldb() {
        let mut program: Vec<u8> = vec![5, 0, 0, 0, 0, 0, 0, 0];
        let mut cpu = Cpu::new();

        cpu.load(&mut program);

        ldb(&mut cpu);

        assert_eq!(5, cpu.rbx);
    }

    #[test]
    fn op_ldb_long() {
        let mut program: Vec<u8> = vec![0xb0, 0x04, 0, 0, 0, 0, 0, 0];
        let mut cpu = Cpu::new();

        cpu.load(&mut program);

        ldb(&mut cpu);

        assert_eq!(1200, cpu.rbx);
    }

    #[test]
    fn op_ldb_neg() {
        let mut program: Vec<u8> = vec![0x50, 0xfb, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff];
        let mut cpu = Cpu::new();

        cpu.load(&mut program);

        ldb(&mut cpu);

        assert_eq!(-1200, cpu.rbx);
    }

    #[test]
    fn op_ldc() {
        let mut program: Vec<u8> = vec![5, 0, 0, 0, 0, 0, 0, 0];
        let mut cpu = Cpu::new();

        cpu.load(&mut program);

        ldc(&mut cpu);

        assert_eq!(5, cpu.rcx);
    }

    #[test]
    fn op_ldc_long() {
        let mut program: Vec<u8> = vec![0xb0, 0x04, 0, 0, 0, 0, 0, 0];
        let mut cpu = Cpu::new();

        cpu.load(&mut program);

        ldc(&mut cpu);

        assert_eq!(1200, cpu.rcx);
    }

    #[test]
    fn op_ldc_neg() {
        let mut program: Vec<u8> = vec![0x50, 0xfb, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff];
        let mut cpu = Cpu::new();

        cpu.load(&mut program);

        ldc(&mut cpu);

        assert_eq!(-1200, cpu.rcx);
    }

    #[test]
    fn op_nop() {
        let mut cpu = Cpu::new();

        nop(&mut cpu);

        assert_eq!(cpu.rax, 0);
        assert_eq!(cpu.rbx, 0);
        assert_eq!(cpu.rcx, 0);

        assert_eq!(cpu.eip, 0);
        assert_eq!(cpu.sp, 0);

        assert_eq!(cpu.flags, 0);
        assert_eq!(cpu.rxx, true);

        assert!(cpu.mem.is_empty());
        assert_eq!(cpu.arg, 0);
    }

    #[test]
    fn op_hlt() {
        let mut cpu = Cpu::new();

        hlt(&mut cpu);

        assert_eq!(cpu.rax, 0);
        assert_eq!(cpu.rbx, 0);
        assert_eq!(cpu.rcx, 0);

        assert_eq!(cpu.eip, 0);
        assert_eq!(cpu.sp, 0);

        assert_eq!(cpu.flags, 0);
        assert_eq!(cpu.rxx, false);

        assert!(cpu.mem.is_empty());
        assert_eq!(cpu.arg, 0);
    }

    #[test]
    fn op_add() {
        let mut cpu = Cpu::new();
        cpu.rax = 2;
        cpu.rbx = 3;

        add(&mut cpu);

        assert_eq!(5, cpu.rax);
    }

    #[test]
    fn op_sub() {
        let mut cpu = Cpu::new();
        cpu.rax = 5;
        cpu.rbx = 3;

        sub(&mut cpu);

        assert_eq!(2, cpu.rax);
    }

    #[test]
    fn op_sia_neg() {
        let mut cpu = Cpu::new();
        let value:isize = -5;
        cpu.arg = value as usize;

        sia(&mut cpu);

        assert_eq!(-5, cpu.rax);
    }

    #[test]
    fn op_sia() {
        let mut cpu = Cpu::new();
        cpu.arg = 5;

        sia(&mut cpu);

        assert_eq!(5, cpu.rax);
    }

    #[test]
    fn op_sib_neg() {
        let mut cpu = Cpu::new();
        let value:isize = -5;
        cpu.arg = value as usize;

        sib(&mut cpu);

        assert_eq!(-5, cpu.rbx);
    }

    #[test]
    fn op_sib() {
        let mut cpu = Cpu::new();
        cpu.arg = 5;

        sib(&mut cpu);

        assert_eq!(5, cpu.rbx);
    }

    #[test]
    fn op_sic_neg() {
        let mut cpu = Cpu::new();
        let value:isize = -5;
        cpu.arg = value as usize;

        sic(&mut cpu);

        assert_eq!(-5, cpu.rcx);
    }

    #[test]
    fn op_sic() {
        let mut cpu = Cpu::new();
        cpu.arg = 5;

        sic(&mut cpu);

        assert_eq!(5, cpu.rcx);
    }

    #[test]
    fn op_jmp() {
        let mut cpu = Cpu::new();
        cpu.arg = 123;

        jmp(&mut cpu);

        assert_eq!(123, cpu.eip);
    }

    #[test]
    fn op_beq_unset() {
        let mut cpu = Cpu::new();
        cpu.arg = 123;
        cpu.flags = 0;

        beq(&mut cpu);

        assert_ne!(123, cpu.eip);
    }

    #[test]
    fn op_beq_set() {
        let mut cpu = Cpu::new();
        cpu.arg = 123;
        cpu.flags = 1;

        beq(&mut cpu);

        assert_eq!(123, cpu.eip);
    }

    #[test]
    fn op_bnz_unset() {
        let mut cpu = Cpu::new();
        cpu.arg = 123;
        cpu.flags = 0;

        bnz(&mut cpu);

        assert_eq!(123, cpu.eip);
    }

    #[test]
    fn op_bnz_set() {
        let mut cpu = Cpu::new();
        cpu.arg = 123;
        cpu.flags = 1;

        bnz(&mut cpu);

        assert_ne!(123, cpu.eip);
    }

    #[test]
    fn op_bgz_unset() {
        let mut cpu = Cpu::new();
        cpu.arg = 123;
        cpu.flags = 0;

        bgz(&mut cpu);

        assert_eq!(123, cpu.eip);
    }

    #[test]
    fn op_bgz_set() {
        let mut cpu = Cpu::new();
        cpu.arg = 123;
        cpu.flags = 2;

        bgz(&mut cpu);

        assert_ne!(123, cpu.eip);
    }

    #[test]
    fn op_blz_unset() {
        let mut cpu = Cpu::new();
        cpu.arg = 123;
        cpu.flags = 2;

        blz(&mut cpu);

        assert_eq!(123, cpu.eip);
    }

    #[test]
    fn op_blz_set() {
        let mut cpu = Cpu::new();
        cpu.arg = 123;
        cpu.flags = 0;

        blz(&mut cpu);

        assert_ne!(123, cpu.eip);
    }
}
