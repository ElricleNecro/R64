use bincode::deserialize;

use std::mem::size_of;

use instructions::*;

static INSTS: &[fn(&mut Cpu)] = &[
    /*     |  0  |  1  |  2  |  3  |  4  |  5  |  6  |  7  |  8  |  9  |  a  |  b  |  c  |  d  |  e  |  f  |     */
    /* 0 */  nop,  sia,  sib,  sic,  lda,  ldb,  ldc,  sta,  stb,  stc,  nop,  nop,  nop,  nop,  nop,  nop, /* 0 */
    /* 1 */  nop,  nop,  nop,  nop,  nop,  nop,  add,  sub,  mul,  div,  mol,  nop,  nop,  nop,  nop,  nop, /* 1 */
    /* 2 */  nop,  nop,  nop,  nop,  beq,  bnz,  bgz,  blz,  nop,  nop,  nop,  jmp,  nop,  nop,  hlt,  nop, /* 2 */
    /* 3 */  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop, /* 3 */
    /* 4 */  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop, /* 4 */
    /* 5 */  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop, /* 5 */
    /* 6 */  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop, /* 6 */
    /* 7 */  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop, /* 7 */
    /* 8 */  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop, /* 8 */
    /* 9 */  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop, /* 9 */
    /* a */  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop, /* a */
    /* b */  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop, /* b */
    /* c */  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop, /* c */
    /* d */  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop, /* d */
    /* e */  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop, /* e */
    /* f */  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop,  nop, /* f */
    /*     |  0  |  1  |  2  |  3  |  4  |  5  |  6  |  7  |  8  |  9  |  a  |  b  |  c  |  d  |  e  |  f  |     */
];

#[derive(Debug)]
pub struct Cpu {
    pub rax: isize,               // Register A
    pub rbx: isize,               // Register B
    pub rcx: isize,               // Register C

    pub eip: usize,               // Instruction pointer
    pub sp: usize,                // Stack pointer

    pub flags: usize,             // Flags
    pub rxx: bool,                // Exit flag

    pub mem: Vec<u8>,        // Program
    pub arg: usize,               // Instruction argument

    loaded: bool,                 // Has a program been loaded?
}

impl Cpu {
    pub fn new() -> Cpu {
        Cpu {
            rax: 0,
            rbx: 0,
            rcx: 0,

            eip: 0,
            sp: 0,

            flags: 0,
            rxx: true,

            mem: Vec::new(),
            arg: 0,

            loaded: false,
        }
    }

    pub fn load(&mut self, program: &Vec<u8>) {
        if program.is_empty() {
            panic!("Program is empty!");
        }

        self.mem.extend(program.iter());
        self.loaded = true;
    }

    pub fn reset(&mut self) {
        self.rax = 0;
        self.rbx = 0;
        self.rcx = 0;

        self.eip = 0;
        self.sp = 0;

        self.flags = 0;
        self.rxx = true;

        self.mem.clear();
        self.arg = 0;

        self.loaded = false;
    }

    fn fetch(&mut self) -> u8 {
        let data_length = self.eip + 1 + size_of::<usize>();
        if data_length < self.mem.len() {
            self.arg = deserialize(&self.mem[(self.eip + 1)..(self.eip + 1 + size_of::<usize>())]).unwrap();
        } else {
            self.arg = 0;
        }

        self.mem[ self.eip ]
    }

    pub fn run(&mut self) {
        if self.mem.is_empty() {
            panic!("There is no program loaded!");
        }

        while self.rxx {
            // println!("DEBUG::Cpu::run: Before: Cpu -> {:?}.", self);
            // Fetch the instruction:
            let opcode = self.fetch() as usize;
            // println!("DEBUG::Cpu::run: Will execute 0x{:x} ({}) at {} with offset: {}.", opcode, opcode, self.eip, OFFSET[opcode]);
            // println!("DEBUG::Cpu::run: Length: {} -- {} -- {}.", self.mem.unwrap().len(), INSTS.len(), OFFSET.len());
            // Increment eip:
            self.eip += OFFSET[ opcode ];
            // Decode & execute it:
            INSTS[ opcode ](self);
            // println!("DEBUG::Cpu::run: After: Cpu -> {:?}.", self);
        }
    }

    pub(crate) fn set_arith_flag(&mut self) {
        if self.rax == 0 {
            self.flags |= 1;
        } else if self.rax < 0 {
            self.flags |= 2;
        } else if self.rax > 0 {
            self.flags &= !2;
        }
    }

    pub(crate) fn get_arith_flag(&self, flag_idx: usize) -> usize {
        let mask: usize = 1 << flag_idx;
        (self.flags & mask) >> flag_idx
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn cpu_new() {
        let cpu = Cpu::new();

        assert_eq!(cpu.rax, 0);
        assert_eq!(cpu.rbx, 0);
        assert_eq!(cpu.rcx, 0);

        assert_eq!(cpu.eip, 0);
        assert_eq!(cpu.sp, 0);

        assert_eq!(cpu.flags, 0);
        assert_eq!(cpu.rxx, true);

        assert!(cpu.mem.is_empty());
    }

    #[test]
    fn cpu_reset() {
        let mut cpu = Cpu::new();
        cpu.rax = 23;
        cpu.rbx = 24;
        cpu.rcx = 25;

        cpu.eip = 2;
        cpu.sp = 9;

        cpu.flags = 45;
        cpu.rxx = false;

        cpu.arg = 10329;

        cpu.reset();

        assert_eq!(cpu.rax, 0);
        assert_eq!(cpu.rbx, 0);
        assert_eq!(cpu.rcx, 0);

        assert_eq!(cpu.eip, 0);
        assert_eq!(cpu.sp, 0);

        assert_eq!(cpu.flags, 0);
        assert_eq!(cpu.rxx, true);

        assert!(cpu.mem.is_empty());
        assert_eq!(cpu.arg, 0);
    }

    #[test]
    fn cpu_load_empty() {
        let cpu = Cpu::new();

        assert!(cpu.mem.is_empty());
    }

    #[test]
    fn cpu_load_full() {
        let mut mem = (0..10).map(|i| i).collect();
        let mut cpu = Cpu::new();

        cpu.load(&mut mem);

        assert!(!cpu.mem.is_empty());
    }

    #[test]
    fn cpu_load_reset() {
        let mut mem = (0..10).map(|i| i).collect();
        let mut cpu = Cpu::new();

        cpu.load(&mut mem);
        cpu.reset();

        assert!(cpu.mem.is_empty());
    }

    #[test]
    #[should_panic(expected = "There is no program loaded!")]
    fn cpu_mem_empty() {
        let mut cpu = Cpu::new();

        cpu.run();
    }

    #[test]
    fn cpu_fetch_short() {
        let program = vec![0x2D];
        let mut cpu = Cpu::new();
        cpu.load(&program);

        let opcode = cpu.fetch();

        assert_eq!(opcode, 0x2D, "Wrong Opcde returned by fetch.");
        assert_eq!(cpu.arg, 0, "As there is not enough spaces, cpu.arg should have been 0.");
    }

    #[test]
    #[should_panic(expected = "Program is empty!")]
    fn cpu_empty_program() {
        let program = Vec::new();
        let mut cpu = Cpu::new();

        cpu.load(&program);
        cpu.run();
    }

    #[test]
    fn cpu_hlt_program() {
        let program = vec![
            0x2E,
        ];
        let mut cpu = Cpu::new();

        cpu.load(&program);
        cpu.run();

        assert_eq!(cpu.rxx, false);
    }

    #[test]
    fn cpu_nop_program() {
        let program = vec![
            0x2D, 0x2E,
        ];
        let mut cpu = Cpu::new();

        cpu.load(&program);
        cpu.run();

        assert_eq!(cpu.rxx, false);
    }

    #[test]
    fn test_set_arith_flag() {
        let mut cpu = Cpu::new();
        cpu.flags = 2;

        cpu.set_arith_flag();

        assert_eq!(cpu.rax, 0);
        assert_eq!(cpu.rbx, 0);
        assert_eq!(cpu.rcx, 0);

        assert_eq!(cpu.eip, 0);
        assert_eq!(cpu.sp, 0);

        assert_eq!(cpu.flags, 3);
        assert_eq!(cpu.rxx, true);

        assert!(cpu.mem.is_empty());
    }

    #[test]
    fn test_get_arith_flag() {
        let mut cpu = Cpu::new();
        cpu.flags = 2;

        let out = cpu.get_arith_flag(1);

        assert_eq!(1, out);

        assert_eq!(cpu.rax, 0);
        assert_eq!(cpu.rbx, 0);
        assert_eq!(cpu.rcx, 0);

        assert_eq!(cpu.eip, 0);
        assert_eq!(cpu.sp, 0);

        assert_eq!(cpu.flags, 2);
        assert_eq!(cpu.rxx, true);

        assert!(cpu.mem.is_empty());
    }
}
